from war.domain.game import Game

import pytest
from datetime import datetime
from freezegun import freeze_time


@pytest.fixture
@freeze_time("2012-01-14")
def game():
    players = ['john', 'mark', 'mary']
    return Game('The great batle', players)


class TestGameCreation:

    def test_game_must_be_in_status_created(self, game):
        assert game.status == 'created'

    @freeze_time("2012-01-14")
    def test_game_must_have_a_creation_datetime(self, game):
        assert game.created_at == datetime.utcnow()
