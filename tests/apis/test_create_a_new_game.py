import pytest
from war.initializers.app import app # noqa


@pytest.fixture
def client():
    client = app.test_client()
    yield client


def test_create_a_new_game(client):
    response = client.post('/games')
    assert response.status_code == 201
