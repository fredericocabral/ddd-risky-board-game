from datetime import datetime
from typing import List
from war.domain.player import Player


class Game:

    def __init__(self, title: str, players: List[Player]) -> None:
        self.title = title
        self.status = 'created'
        self.created_at = datetime.utcnow()
