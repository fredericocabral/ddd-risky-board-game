from flask import jsonify, current_app as app


@app.route('/games', methods=['GET', 'POST'])
def games():
    data = {'hello': 'world'}
    return jsonify(data), 201
